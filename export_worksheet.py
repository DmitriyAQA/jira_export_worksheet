from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import csv
import datetime
from dateutil.relativedelta import relativedelta
import os.path
import glob


def test_export():
    # open csv
    with open("jira_value.csv", "r") as fh:
        lines = csv.reader(fh)
        for line in lines:
            jira_user = line[0]
            jira_password = line[1]
            download_path = line[2]
            date_from = line[3]
            bat_file_path = line[4]

        # chrome options
    chromeOptions = Options()
    chromeOptions.headless = True
    chromeOptions.add_experimental_option('prefs', {'download.default_directory': download_path,
                                                    'download.prompt_for_download': False,
                                                    'profile.default_content_setting_values.automatic_downloads': 1,})
    driver = WebDriver(executable_path='C://selenium//chromedriver.exe', chrome_options=chromeOptions)
    #driver.maximize_window()

    # date calculation
    end_date_for_url = datetime.datetime.utcnow().strftime('%Y-%m-%d')
    end_date = datetime.datetime.utcnow()
    from_date = end_date - relativedelta(days=int(date_from))
    from_date_for_url = from_date.strftime('%Y-%m-%d')

    # checking for file existence
    path_report = '%s%s%s%s%s%s%s' % (download_path, '\Report_', from_date_for_url, '_', end_date_for_url, '_Logged_Time', '.csv')
    if os.path.exists(path_report):
        print('The Report already loaded, please check the folder')
        driver.close()
    else:
        ()

    # URL
    driver.get(
        'https://jet-bi.atlassian.net/plugins/servlet/ac/io.tempo.jira/tempo-app#!/reports/logged-time?columns=WORKED_COLUMN&dateDisplayType=days&from='
        '{}&groupBy=project&groupBy=worker&order=ASCENDING&periodKey&periodType=FIXED&showCharts=true&sortBy='
        'TITLE_COLUMN&subPeriodType=MONTH&to={}&viewType=TIMESHEET'.format(from_date_for_url, end_date_for_url))
    # jira sign in button
    sign_in_button = WebDriverWait(driver, 10).until(ec.element_to_be_clickable(
        (By.XPATH, '//*[@id="jira-frontend"]/div[1]/div/div[1]/div/header/div/a/div/button/span/span'))).click()

    # login with google
    # cont_with_google = WebDriverWait(driver, 10).until(
    # ec.element_to_be_clickable((By.XPATH, '//*[@id="google-auth-button"]/span/span'))).click()

    # input jira credentials
    jira_email_input = WebDriverWait(driver, 10).until(ec.element_to_be_clickable((By.ID, 'username'))).send_keys(
        jira_user)
    jira_cont_button = driver.find_element_by_css_selector('#login-submit > span > span').click()
    google_pass_input = WebDriverWait(driver, 10).until(ec.element_to_be_clickable((By.ID, 'password'))).send_keys(
        jira_password)
    jira_login_button = driver.find_element_by_css_selector('#login-submit > span > span').click()
    WebDriverWait(driver, 30).until(ec.visibility_of_all_elements_located((By.CSS_SELECTOR, 'iframe')))

    # export of report
    driver.switch_to.frame(0)
    export_button = WebDriverWait(driver, 20).until(
        ec.element_to_be_clickable((By.CSS_SELECTOR, '#exportDropdown'))).click()
    export_data = WebDriverWait(driver, 10).until(ec.element_to_be_clickable((By.XPATH,
                                                                              '//*[@id="tempo-nav"]/div[2]/div[2]/div[3]/div/div/div[3]'))).click()

    # Wait for download
    while True:
        download_folder = os.path.expanduser(download_path)
        file_name = '%s%s%s%s%s%s' % ('\Report_', from_date_for_url, '_', end_date_for_url, '_Logged_Time', '.csv')
        filenames = glob.glob(download_folder + file_name)
        if len(filenames) > 0 and not any('.crdownload' in name for name in filenames):
            break
        for name in filenames:
            if name.endswith('.crdownload'):
                continue
            if name.endswith('.csv'):
                break
            else:
                break

    # Writing path to file
    file = open(bat_file_path, 'w')
    file.write(download_path)
    file.close()
    print('The report path has been successfully added to the file')

    driver.close()

#TODO:
